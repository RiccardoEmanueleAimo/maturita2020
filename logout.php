<?php
session_start();
if (isset($_SESSION['googleCode'])):
    session_regenerate_id();
    unset($_SESSION['googleCode']);
    unset($_SESSION['username']);
    unset($_SESSION['secret']);

    session_destroy();
	header("location:login.php");
endif;
?>

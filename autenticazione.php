<?php
include("connessione.php");

$checkResult="";
if($_POST['otp']){
$otp=$connect->real_escape_string($_POST['otp']);
$secret = $_SESSION['secret'];

require_once 'googleLib/GoogleAuthenticator.php';
$ga = new GoogleAuthenticator();
$checkResult = $ga->verifyCode($secret, $otp, 2);    // 2 = 2*30sec tolleranza


if ($checkResult){
	$_SESSION['googleCode']	= $otp;
	header("location:account.php");
    exit;

}
else{
	header("location:conferma_2fa_login.php");
    exit;
}

}

?>

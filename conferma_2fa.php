<?php

include('connessione.php');

$secret = $_SESSION['secret'];
$user 	= $_SESSION['username'];

require_once 'googleLib/GoogleAuthenticator.php';
$ga 		= new GoogleAuthenticator();
$qrCodeUrl 	= $ga->getQRCodeGoogleUrl($user, $secret,'Covid-19');

?>

<!doctype html>
<html lang="it">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Covid-19 App</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/layout.css">
        <link rel="stylesheet" href="assets/css/form-design.css">
		    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <style>
          .navbar-center
           {
             position: absolute;
             width: 100%;
             left: -5.3%;
             top: 0;
             text-align: center;
           }

           .a2z-wrapper{
           	    font-family: 'Roboto', sans-serif;
                font-size: 14px;
                line-height: 26px;
                font-weight: 400;
                color: #353940;
           		  background: url(assets/img/bg.png);
                overflow: hidden;
           }
           .a2z-area{
              position: fixed;
              width: 100%;
              height: 100%;
              top:8%;
           }
        </style>


</head>
    <body class="a2z-wrapper">


        <!--Start a2z-area-->
        <section class="a2z-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-area register-from" style="box-shadow: 0 0 40px 0 #000000;">
                            <div class="form-content">
                                <h2 style="text-transform:none;">Autenticazione a 2 Fattori</h2>
                                <p style="text-transform:none;">Inserisci il codice di verifica generato dall'app Google Authenticator sul telefono.</p>
                                <ul>
                                    <li><a class="btn btn-block btn-social" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en">
										<img src="assets/img/android.png">
									  </a></li>
									 <li> <a class="btn btn-block btn-social" href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank">
										<img src="assets/img/iphone.png">
									  </a></li>

                                </ul>
                            </div>
                            <div class="form-input">
                                <h2>Inserisci il Codice</h2>
                                <form name="reg" action="autenticazione.php" method="POST">

                                    <div class="form-group">
										<img src='<?php echo $qrCodeUrl; ?>'/>
                                    </div>

                                    <div class="form-group">
										<input type="text" name="otp" id="otp" autocomplete="off" value="" required>
                                        <label>Inserisci il codice OTP</label>
                                    </div>

                                    <div class="a2z-button">
                                        <button type="submit" class="a2z-btn">Invia</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Barra di Navigazione -->

        <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%;position: fixed; top: 0;">
        <!-- Brand/logo -->
        <a class="navbar-brand" style="position:relative; top: 50%;" href="index.php">
          <img src="assets/img/coronavirus.png" alt="logo" >
        </a>

        <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="index.php">
          <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
        </a>

        </nav>

    </body>
</html>

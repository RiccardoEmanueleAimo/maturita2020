<?php
include_once('class/data.class.php');
include_once('config/config.php');

$mysqli = new data("localhost", "aimo2020","" , "my_aimo2020");
$province = $mysqli->getProvince('01');
$mysqli->close();

include('connessione.php');

$csrf		= $connect->real_escape_string($_POST["csrf"]);

require_once 'googleLib/GoogleAuthenticator.php';

$ga = new GoogleAuthenticator();
$secret = $ga->createSecret();

if (hash_equals($_SESSION['token'], $csrf)) {

  $googlecode = $connect->real_escape_string($_POST['googlecode']);
  $username   = $connect->real_escape_string($_POST['username']);
  $password   = $connect->real_escape_string($_POST['password']);
  $anno       = $connect->real_escape_string($_POST['anno']);
  $genere     = $connect->real_escape_string($_POST['genere']);
  $provincia  = $connect->real_escape_string($_POST['provincia']);
  $comune     = $connect->real_escape_string($_POST['comune']);


  /* Controllo dell'univocità dell'Username e inserimento nel database*/
    $query		= db_query("select * from  utente where id='".$username."'");
    $univoco  = mysqli_num_rows($query);

    if($univoco > 0){
		header('Location:register.php');
    echo "<script>document.getElementById('username').innerHTML = 'Utente già esistente!';</script>";
		exit();
	}else{
                            $tab_nome = "utente";
                        		$crypt = md5($password);

                        		$SQL = "INSERT INTO $tab_nome(id,password,anno,genere,provincia,comune,googlecode)";
                        		$SQL .= "VALUES ('";
                        		$SQL .= $username;
                        		$SQL .= "','";
                        		$SQL .= $crypt;
                        		$SQL .= "','";
                        		$SQL .= $anno;
                        		$SQL .= "','";
                        		$SQL .= $genere;
                        		$SQL .= "','";
                        		$SQL .= $provincia;
                        		$SQL .= "','";
                        		$SQL .= $comune;
                        		$SQL .= "','";
                        		$SQL .= $googlecode;
                            $SQL .= "');";

    	$mysql = db_query($SQL);

		$_SESSION['username'] 	= $username;
		$_SESSION['secret']     = $googlecode;

		header('Location:conferma_2fa.php');
		exit();
	}

}

$connect->close();

?>

<!doctype html>
<html lang="it">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Covid-19 App</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/layout.css">
        <link rel="stylesheet" href="assets/css/form-design.css">
				<link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/italia.js"></script>
        <script src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>

        <style>
          .navbar-center
           {
             position: absolute;
             width: 100%;
             left: -5.3%;
             top: 0;
             text-align: center;
           }

           .a2z-wrapper{
           	    font-family: 'Roboto', sans-serif;
                font-size: 14px;
                line-height: 26px;
                font-weight: 400;
                color: #353940;
           		  background: url(assets/img/bg.png);
                overflow: hidden;
           }
           .a2z-area{
              position: fixed;
              width: 100%;
              height: 100%;
              top:4%;
           }
        </style>

      </head>


    <body class="a2z-wrapper">

      <!--Start a2z-area-->
      <section class="a2z-area">
          <div class="container">
              <div class="row justify-content-center">
                  <div class="col-lg-8">
                      <div class="form-area register-from" style="box-shadow: 0 0 40px 0 #000000;">
                          <div class="form-content" >
                              <h2 style="text-transform:none;">Autenticazione a 2 Fattori</h2>
                              <p style="text-transform:none;">Per forinirti la migliore sicurezza possibile ti invitiamo a scaricare Google Authenticator dai link sottostanti, grazie!</p>
                              <ul>
                                <li><a class="btn btn-block btn-social" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en">
                                  <img src="assets/img/android.png">
                                </a>
                              </li>
                              <li> <a class="btn btn-block btn-social" href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank">
                                <img src="assets/img/iphone.png">
                              </a>
                            </li>

                          </ul>
                        </div>

      <div class="form-input">
          <h2 style="text-transform:none;">Registrazione</h2>

                <!-- FORM -->
                <form name="reg" action="registrazione.php" method="POST">
                          <!-- VALORI AUTENTICATORE -->
								          <input type="hidden" name="csrf" 	 value="<?php print $_SESSION["token"]; ?>" >
								          <input type="hidden" name="googlecode" value="<?php echo $secret; ?>" >
                            <?php $utente = getRandomString() ?>

                            <label style="position: relative;" >Il tuo username:</label>
                            <div class="form-group">
                             <input style="border-bottom: 2px solid #2D87D7;" type="text" name="username" id="username" autocomplete="off" value="<?php echo $utente ?>" required readonly />
                            </div>

                            <div class="form-group">
                            <input type="password" name="password" id="password" autocomplete="off" value="" required>
                              <label>Password</label>
                            </div>

                            <div class="row">
                             <div class="col-lg-6">
                              <div class="form-group">
                               <input type="number" name="anno" id="anno" min="1900" max="2020" step="1" autocomplete="off" value="" required />
                                <label>Anno di Nascita</label>
                             </div>
                            </div>

                            <div class="form-group" >
                             <input style="width: 25px; position: relative; top:15%;" type="radio" name="genere" value="m" >
                             <input style="width: 25px; position: relative; top:15%; left: 30%;" type="radio" name="genere" value="f" ><br>
                               <label>Genere</label>
                               Maschio &ensp; Femmina
                             </div>
                            </div>

                            <p><label for="provincia">Provincia</label>
                            <select name="provincia" id="provincia" class="dinamiche">
                            	<option value="">Seleziona...</option>
                              <?php foreach($province as $val): ?>
                                <option value="<?php echo $val['codice']; ?>"><?php echo $val['nome']; ?></option>
                                <?php endforeach; ?>
                            </select></p>

                            <p><label for="comune">Comune</label>
                            <select name="comune" id="comune">
                            	<option value="">Seleziona...</option>
                            </select></p>

                          <div class="a2z-button">
                           <button type="submit" class="a2z-btn">Crea Account</button>
                            </div>

								   	      <div class="form-text">
                            <span>Sei già registrato?  <a href="login.php">Accedi</a></span>
                          </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Barra di Navigazione -->

        <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%; position: fixed; top: 0; ">
        <!-- Brand/logo -->
        <a class="navbar-brand" style="position:relative; top: 50%;" href="index.php">
          <img src="assets/img/coronavirus.png" alt="logo" >
        </a>

        <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="index.php">
          <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
        </a>

        </nav>

    </body>
</html>

<?php

function getRandomString($length = 8) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $string = '';

  for ($i = 0; $i < $length; $i++) {
      $string .= $characters[mt_rand(0, strlen($characters) - 1)];
  }

  return $string;
}

 ?>

<?php
include('connessione.php');
if (!isset($_SESSION['googleCode'])):
    header("location:registrazione.php");
	exit();
endif;

$csrf		= $connect->real_escape_string($_POST["csrf"]);

if (hash_equals($_SESSION['token'], $csrf)) {

  $username   = $connect->real_escape_string($_POST['username']);
  if(isset($_POST["malato"]))
  		{
  		  $malato = 'm';
  		}else{
  		  $malato = 's';
  		}
  $temp   = $connect->real_escape_string($_POST['temperatura']);
  $pres   = $connect->real_escape_string($_POST['pressione']);
  $sat    = $connect->real_escape_string($_POST['saturazione']);
  $data_r = date("Y-m-d");
  $ora_r  = date("h:i:s");

  $tab_nome = "stato";

  $SQL = "INSERT INTO $tab_nome(data_r,ora_r,malato,temp,pres,sat,id_u)";
  $SQL .= "VALUES ('";
  $SQL .= $data_r;
  $SQL .= "','";
  $SQL .= $ora_r;
  $SQL .= "','";
  $SQL .= $malato;
  $SQL .= "','";
  $SQL .= $temp;
  $SQL .= "','";
  $SQL .= $pres;
  $SQL .= "','";
  $SQL .= $sat;
  $SQL .= "','";
  $SQL .= $username;
  $SQL .= "');";

$mysql = db_query($SQL);

if(isset($_POST['testa'])){
$query = "INSERT INTO sintomo_utente(id_s,id_u) values('1','$username'); ";
$mysql = db_query($query);
//echo $query;
}
if(isset($_POST['gola'])){
$query = "INSERT INTO sintomo_utente(id_s,id_u) values('2','$username'); ";
$mysql = db_query($query);
//echo $query;
}
if(isset($_POST['febbre'])){
  $query = "INSERT INTO sintomo_utente(id_s,id_u) values('3','$username'); ";
  $mysql = db_query($query);
//  echo $query;
}
header("Location: account.php");


}


$connect->close();



?>
<!doctype html>
<html lang="it">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Covid-19 App
        </title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/layout.css">
        <link rel="stylesheet" href="assets/css/form-design.css">
		    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <style>
          .navbar-center
           {
             position: absolute;
             width: 100%;
             left: -5.3%;
             top: 0;
             text-align: center;
           }

           .a2z-wrapper{
           	    font-family: 'Roboto', sans-serif;
                font-size: 14px;
                line-height: 26px;
                font-weight: 400;
                color: #353940;
           		  background: url(assets/img/bg.png);
                overflow: hidden;
           }
           .a2z-area{
              position: fixed;
              width: 100%;
              height: 100%;
              top:7%;
           }
        </style>

</head>

    <body class="a2z-wrapper">

        <!--Start a2z-area-->
        <section class="a2z-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-area login-form" style="box-shadow: 0 0 40px 0 #000000;">
                            <div class="form-content">
                              <h2 style="text-transform:none;">Inserimento Dati sullo stato di salute </h2>
                              <p style="text-transform:none;">Aggiornamento Dati: 1 volta al giorno se si è negativi al virus, 2 volte se invece si è risultati positivi.</p>

                            </div>
                            <div class="form-input">
                                <h2>Form di inserimento dati</h2>
                                <form name="reg" action="insert.php" method="POST">

              	                   <input type="hidden" name="csrf" 	     value="<?php print $_SESSION["token"]; ?>" >
                                   <input type="hidden" name="username" 	 value="<?php print $_SESSION["username"]; ?>" >
                                    <div class="form-group">
										                    <input type="checkbox" name="malato" id="malato" value="m" class="checkbox" style="width: 25px; position: relative; top:15%;" />
                                        <label class="nt" style="display:inline; left:10%; top:22%;">Sei risultato positivo al covid-19?</label>
                                    </div>

                                    <ul id="parametri">
                                    <div class="form-group" >
                                        <input type="number" id="temperatura" name="temperatura" min="28" max="50" step="0.01" style="padding:5px;">
                                        <label>Temperatura corporea</label>
                                  </div>

                                  <div class="form-group" >
                                    <input type="number" id="pressione" name="pressione" min="50" max="200" step="0.01" style="padding:5px;">
                                    <label>Pressione Massima Arteriosa</label>
                                  </div>

                                  <div class="form-group" >
                                    <input type="number" id="saturazione" name="saturazione" min="0" max="100" style="padding:5px;">
                                    <label>Percentuale Saturazione Sanguigna</label>
                                  </div>

                                  <p>Sintomi:</P>

                                  <div class="form-group" style="margin-bottom: 0px; height:50px;">
                                    <input type="checkbox" class="sintomi" name="febbre" value="febbre" style="width: 25px; position: relative; top:15%;"style="width: 25px; position: relative; top:15%;"style="width: 25px; position: relative; top:15%;">
                                    <label style="display:inline; left:10%; top:50%;">Febbre</label>
                                  </div>

                                  <div class="form-group" style="margin-bottom: 0px; height:50px;">
                                    <input type="checkbox" class="sintomi" name="testa" value="mal di testa" style="width: 25px; position: relative; top:15%;"style="width: 25px; position: relative; top:15%;">
                                    <label style="display:inline; left:10%; top:50%;">Mal di Testa</label>
                                  </div>

                                  <div class="form-group" style="margin-bottom: 0px; height:50px;">
                                    <input type="checkbox" class="sintomi" name="gola" value="mal di gola" style="width: 25px; position: relative; top:15%;" >
                                    <label style="display:inline; left:10%; top:50%;">Mal di Gola</label>
                                  </div>

                                </ul>

                                    <div class="a2z-button">
                                        <button  class="a2z-btn">Invia</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--End a2z-area-->

        <!-- Barra di Navigazione -->

        <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%; position: fixed; top: 0; ">
        <!-- Brand/logo -->
        <a class="navbar-brand" style="position:relative; top: 50%;" href="index.php">
          <img src="assets/img/coronavirus.png" alt="logo" >
        </a>

        <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="index.php">
          <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
        </a>

        </nav>

        <!-- jquery  -->
        <script src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/check.js"></script>
        <!-- Bootstrap js  -->
        <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>

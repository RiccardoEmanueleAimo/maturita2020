<?php
include('connessione.php');
if (!isset($_SESSION['googleCode'])):
    header("location:registrazione.php");
	exit();
endif;

$googlecode = $_SESSION['secret'];
$sql = db_query("select u.id, p.provincia, c.comune from utente u,province p,comuni c where googlecode = '".$googlecode."' and u.provincia=p.cod_provincia and u.comune = c.cod_istat;");
$row = mysqli_fetch_array($sql);

$username 	= $row['id'];
$provincia 	= $row['provincia'];
$comune 		= $row['comune'];

?>

<!doctype html>
<html lang="it">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Covid-19 App</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/layout.css">
        <link rel="stylesheet" href="assets/css/form-design.css">
		    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <style>
          .navbar-center
           {
             position: absolute;
             width: 100%;
             left: -5.3%;
             top: 0;
             text-align: center;
           }

           .a2z-wrapper{
           	    font-family: 'Roboto', sans-serif;
                font-size: 14px;
                line-height: 26px;
                font-weight: 400;
                color: #353940;
           		  background: url(assets/img/bg.png);
                overflow: hidden;
           }
           .a2z-area{
              position: fixed;
              width: 100%;
              height: 100%;
              top:4%;
           }
        </style>
</head>
    <body class="a2z-wrapper">

        <!--Start a2z-area-->
        <section class="a2z-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-area register-from" style="box-shadow: 0 0 40px 0 #000000;">
                            <div class="form-content">
                                <h2>Benvenuto <?php print $username; ?></h2>
                                <p>Grazie per utilizzare il nostro servizio!</p>
                                <div class="a2z-button" >
                                                      <a href="insert.php" class="a2z-btn" style="padding: 10px 30px; ">Inserisci</a>
                                                  </div>
                                <div class="a2z-button" >
                                                      <a href="update.php" class="a2z-btn" style="padding: 10px 30px; ">Modifica</a>
                                                  </div>
                                <div class="a2z-button" >
                                                      <a href="select.php" class="a2z-btn" style="padding: 10px 30px; ">Visualizza</a>
                                                  </div>
                            </div>
                            <div class="form-input">
                                <h2>Benvenuto <?php print $username; ?></h2>
                                    <div class="row">
										<div class="form-group">
											<label>Codice Username: </label> <span style="color:#2d87d7"><?php print $username; ?></span>
										</div>
                                    </div>

									<div class="row">
										<div class="form-group">
											<label>Provincia: </label> <span style="color:#2d87d7"><?php print $provincia; ?></span>
										</div>
                                    </div>

									<div class="row">
										<div class="form-group">
											<label>Comune: </label> <span style="color:#2d87d7"><?php print $comune; ?></span>
										</div>
									</div>

									<div class="a2z-button">
                                        <a href="logout.php" class="a2z-btn" style="padding: 10px 30px;">Disconnettiti</a>
                                    </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Barra di Navigazione -->

        <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%; position: fixed; top: 0; ">
        <!-- Brand/logo -->
        <a class="navbar-brand" style="position:relative; top: 50%;" href="index.php">
          <img src="assets/img/coronavirus.png" alt="logo" >
        </a>

        <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="index.php">
          <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
        </a>

        </nav>


        <script src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

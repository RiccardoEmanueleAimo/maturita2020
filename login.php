<?php
include('connessione.php');

$csrf		=	$connect->real_escape_string($_POST["csrf"]);

if (hash_equals($_SESSION['token'], $csrf)) {
	$username	= $connect->real_escape_string($_POST['username']);
	$password	= $connect->real_escape_string($_POST['password']);

  $password = md5($password);

	/* Controllo Username e Password */
	$query		= db_query("select * from utente where id='".$username."' and password='".$password."' ");

	$controllo = mysqli_num_rows($query);
	if($controllo > 0){
		$row = mysqli_fetch_array($query);
		$_SESSION['username'] 	= $_POST['username'];
		$_SESSION['secret']     = $row['googlecode'];

		header('Location:conferma_2fa_login.php');
		exit();
	}else{
		header('Location:login.php?error=1');
		exit();
	}

}

$msg = $connect->real_escape_string($_GET["error"]);
if($msg == 1){ $strmsg = "Credenziali inserite errate!"; }


?>
<!doctype html>
<html lang="it">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Covid-19 App
        </title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/layout.css">
        <link rel="stylesheet" href="assets/css/form-design.css">
		    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <style>
          .navbar-center
           {
             position: absolute;
             width: 100%;
             left: -5.3%;
             top: 0;
             text-align: center;
           }

           .a2z-wrapper{
           	    font-family: 'Roboto', sans-serif;
                font-size: 14px;
                line-height: 26px;
                font-weight: 400;
                color: #353940;
           		  background: url(assets/img/bg.png);
                overflow: hidden;
           }
           .a2z-area{
              position: fixed;
              width: 100%;
              height: 100%;
              top:12%;
           }
        </style>
</head>

    <body class="a2z-wrapper">

        <!--Start a2z-area-->
        <section class="a2z-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="form-area login-form" style="box-shadow: 0 0 40px 0 #000000;">
                            <div class="form-content">
                              <h2 style="text-transform:none;">Autenticazione a 2 Fattori</h2>
                              <p style="text-transform:none;">Per forinirti la migliore sicurezza possibile ti invitiamo a scaricare Google Authenticator dai link sottostanti, grazie!</p>
                                <ul>
                                    <li><a class="btn btn-block btn-social" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en">
										<img src="assets/img/android.png">
									  </a></li>
									 <li> <a class="btn btn-block btn-social" href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8" target="_blank">
										<img src="assets/img/iphone.png">
									  </a></li>

                                </ul>
                            </div>
                            <div class="form-input">
                                <h2>Form di Login</h2>
								                <span class="error"><?php print $strmsg; ?></span>
                                <form name="reg" action="login.php" method="POST">

              	                   <input type="hidden" name="csrf" 	 value="<?php print $_SESSION["token"]; ?>" >
                                    <div class="form-group">
										<input type="text" name="username" id="username" autocomplete="off" value="" required>
                                        <label>Username</label>
                                    </div>
                                    <div class="form-group">
										<input type="password" name="password" id="password" autocomplete="off" value="" required>
                                        <label>Password</label>
                                    </div>
                                    <div class="a2z-button">
                                        <button class="a2z-btn">Accedi</button>
                                    </div>

									 <div class="form-text text-right">
                                        Non sei registrato? <a href="registrazione.php">Crea il tuo account </a>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--End a2z-area-->

        <!-- Barra di Navigazione -->

        <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%; position: fixed; top: 0; ">
        <!-- Brand/logo -->
        <a class="navbar-brand" style="position:relative; top: 50%;" href="index.php">
          <img src="assets/img/coronavirus.png" alt="logo" >
        </a>

        <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="index.php">
          <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
        </a>

        </nav>

        <!-- jquery  -->
        <script src="assets/js/jquery-1.12.4.min.js"></script>
        <!-- Bootstrap js  -->
        <script src="assets/css/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

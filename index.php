<!DOCTYPE html>
<html>

  <head>
      <title>Covid-19 App</title>
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/layout.css">
      <link rel="stylesheet" href="assets/css/form-design.css">
      <link rel="stylesheet" href="assets/css/font-awesome.min.css">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
       @font-face { font-family: ColorTube; src: url('assets/fonts/ColorTube-Regular.ttf'); }
       .navbar-center
        {
          position: absolute;
          width: 100%;
          left: 0;
          top: 0;
          text-align: center;
        }
        .accedi{
          border-radius: 100px;
          background: rgba(45,135,215);
          border: none;
          color: white;
          padding: 20px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 22px;
          margin: 4px 2px;
          cursor: pointer;
          float:right;
          position: relative;
          right: 2%;
          width: 250px;
        }
        body{
          background-image: url('assets/img/bg.png');
        }
        i{
          margin-right: 16%;
          margin-left: 16%;
          position:absolute;
          top: 68%;
          text-align:center;
          font-family: ColorTube, sans-serif;
          font-size:20px;
          color: #ffa500;
        }
      </style>
  </head>

<body>

  <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%;">
  <!-- Brand/logo -->
  <a class="navbar-brand" style="position:relative; top: 50%;" href="#">
    <img src="assets/img/coronavirus.png" alt="logo" >
  </a>

  <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="#">
    <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
  </a>

  <button class="accedi" value="login" onclick="window.location.href='registrazione.php';">Accesso Web</button>

  </nav>

<p style="position:absolute; top: 50%; text-align:center; width:100%; font-family: ColorTube, sans-serif; font-size:60px; color: #fff;  "> Ripartiamo Insieme </p>
<i> Tornare alla propria vita senza rischi è possibile: Covid-19 e' un' applicazione per chi non vuole rinunciare alla propria privacy </i>

</body>
</html>

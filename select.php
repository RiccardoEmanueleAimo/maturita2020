<?php
include('connessione.php');
if (!isset($_SESSION['googleCode'])):
    header("location:registrazione.php");
	exit();
endif;

 ?>


<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Covid-19 App
  </title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/layout.css">
  <link rel="stylesheet" href="assets/css/form-design.css">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">

  <style>
    .navbar-center
     {
       position: absolute;
       width: 100%;
       left: -5.3%;
       top: 0;
       text-align: center;
     }
     body{
       font-family: 'Roboto', sans-serif;
       font-size: 14px;
       line-height: 26px;
       font-weight: 400;
       color: #353940;
       background: url(assets/img/bg.png);
       overflow: hidden;
     }
     table {
      border-collapse: collapse;
      width: 60%;
      margin: 0 auto;
      position: relative;
      top:40%;
    }

    th, td {
      text-align: left;
      padding: 8px;
    }
    tr{
      background-color: #25ccc1;
    }

    tr:nth-child(even){background-color: #f2f2f2}

    th {
      background-color: rgba(45,135,215);
      color: white;
    }
     </style>


</head>

<body>

  <nav class="navbar navbar-expand-sm bg-transparent navbar-light" style="width:100%; position: fixed; top: 0; ">
  <!-- Brand/logo -->
  <a class="navbar-brand" style="position:relative; top: 50%;" href="index.php">
    <img src="assets/img/coronavirus.png" alt="logo" >
  </a>

  <a class="navbar-center" style="position:relative; top: 50%; text-decoration:none; " href="index.php">
    <span style="font-family: ColorTube, sans-serif; font-size:90px; color: rgba(45,135,215);"> Covid-19 </span>
  </a>

  </nav>


<table>
    <tr>
        <th>Username</th>
        <th>Condizione di salute</th>
        <th>Temperatura</th>
        <th>Pressione Sanguigna</th>
        <th>Saturazione Sanguigna</th>
        <th>Data ultimo agg.</th>
        <th>Ora ultimo agg.</th>
    </tr>
<?php


$tab_nome = 'stato';
$username = $_SESSION['username'];
$sql = "SELECT * FROM $tab_nome where id_u='$username' ORDER BY data_r,ora_r ";
$result = db_query($sql);

while ($row = $result->fetch_assoc()){

if($row['malato'] == 'm'){
	$malato = 'Positivo';
}else{if($row['malato'] == 's'){$malato = 'Negativo';}}

echo " <tr>";
echo " <td>" . $row['id_u'] . " </td> ";
echo " <td>" . $malato .  "</td> ";
echo " <td>".  $row['temp'] . "</td> ";
echo " <td>" . $row['pres'] . "</td> ";
echo " <td>" . $row['sat'] . "</td> ";
echo " <td>" . $row['data_r'] . " </td> ";
echo " <td>" . $row['ora_r'] . " </td> ";
echo " </tr>";
}
$result->free();
$conn->close();


?>

    </table>


  </body>
</html>
